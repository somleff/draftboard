<?php

/*
|--------------------------------------------------------------------------
| Site Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'PagesController@main');

Route::get('/blog', 'PagesController@blog');

Route::get('/albums', 'PagesController@albums');

Route::get('/contacts', 'PagesController@contactUs');
    Route::post('/contacts/store', 'ContactController@store');

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/dashboard'], function (){
    // Main
    Route::get('/main', 'BlockController@index')->name('main.index');
    Route::get('/main/{block}/edit', 'BlockController@edit')->name('main.edit');
    Route::patch('/main/{block}/update', 'BlockController@update')->name('main.update');

    // Contacts
    Route::get('/contacts', 'ContactController@index')->name('contact.index');
    Route::get('/contacts/{contact}', 'ContactController@show')->name('contacts.show');
    Route::delete('/contacts/{contact}/delete', 'ContactController@destroy')->name('contacts.destroy');

    // Albums
    Route::get('/albums', 'AlbumController@index')->name('albums.index');
    Route::get('/albums/create', 'AlbumController@create')->name('albums.create');
    Route::post('/albums/store', 'AlbumController@store')->name('albums.store');
//
//    Route::get('/blog', function (){
//        return view('dashboard.pages.blog');
//    });
//
//    Route::get('/gallery', function (){
//        return view('dashboard.pages.gallery');
//    });
});