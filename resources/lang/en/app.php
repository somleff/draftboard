<?php

return [
    // Contacts Messages
    'contacts_store' => 'Your message was sent successfully. Thanks!',
    'contacts_destroy' => 'The contact was successfully deleted.',

    // Main Block Message
    'block_update' => 'The block was successfully updated.',

    // Albums Messages
    'album_store' => 'The album was successfully created.',

];
