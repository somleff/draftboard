@if (session('status'))
    <div class="alert alert-dismissible alert-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h6 class="alert-heading">Success!</h6>
        <p class="mb-0">
            {{ session('status') }}
        </p>
    </div>
@endif
