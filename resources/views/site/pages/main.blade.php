@extends('site.layouts.master')

@section('title', 'Main Page')

@section('content')
    @forelse ($blocks as $block)
        <div class="jumbotron">
            <h1 class="display-3">
                {{ $block->title }}
            </h1>

            <p class="lead">
                {{ $block->description }}
            </p>

            <hr class="my-4">
            <p>
                {{ $block->text }}
            </p>
        </div>
    @empty
        <div class="alert alert-dismissible alert-info">
            <strong>Heads up!</strong> We will back soon!
        </div>
    @endforelse
@endsection
