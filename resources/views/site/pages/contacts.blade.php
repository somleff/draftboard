@extends('site.layouts.master')

@section('title', 'Contact Us')

@section('content')
    <div class="card bg-light mb-3 mx-auto" style="max-width: 25rem;">
        <div class="card-header">Leave Your Contact</div>
        <div class="card-body">
            <form method="POST" action="/contacts/store">
                @csrf
                <fieldset>
                    <div class="form-group">
                        <label for="name">
                            Name
                        </label>
                        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                               value="{{ old('name') }}" name="name" autofocus required>
                        {{-- Display errors --}}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">
                            Email
                        </label>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}" name="email" required>
                        {{-- Display errors --}}
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="phone">
                            Phone
                        </label>
                        <input type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                               value="{{ old('phone') }}" name="phone" required>
                        {{-- Display errors --}}
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="message">
                            Message
                        </label>
                        <textarea class="form-control {{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" rows="6" required>{{ old('message') }}</textarea>
                        {{-- Display errors --}}
                        @if ($errors->has('message'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="row justify-content-center">
                        <button type="submit" class="btn btn-warning">Send</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <div class="row justify-content-end">
        <div class="col-3">
            {{-- If send Feedback was successful --}}
            @include('elements.status')
        </div>
    </div>
@endsection
