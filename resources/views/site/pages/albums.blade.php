@extends('site.layouts.master')

@section('title', 'Gallery Page')

@section('content')
    <div class="row">
        @forelse ($albums as $album)
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <h3 class="card-header">
                        {{ $album->name }}
                    </h3>

                    <img class="card-img-top" src="/storage/{{ $album->cover_img_path }}"
                         alt="Card image cap"
                    >

                    <div class="card-body">
                        <p class="card-text">
                            {{ $album->description }}
                        </p>

                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-dismissible alert-warning">
                <h4 class="alert-heading">We're sorry!</h4>
                <p class="mb-0">There is no albums yet.</p>
            </div>
        @endforelse
    </div>
        {{ $albums->links() }}
@endsection
