<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="">
            Draft Board
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                    <a class="nav-link" href="/">Main</a>
                </li>

                <li class="nav-item {{ Request::is('blog') ? 'active' : '' }}">
                    <a class="nav-link" href="/blog">Blog</a>
                </li>

                <li class="nav-item {{ Request::is('gallery') ? 'active' : '' }}">
                    <a class="nav-link" href="/albums">Albums</a>
                </li>

                <li class="nav-item {{ Request::is('contacts') ? 'active' : '' }}">
                    <a class="nav-link" href="/contacts">Contact Us</a>
                </li>
            </ul>

            <a class="nav-link" href="/dashboard/main">Dashboard</a>
        </div>
    </div>
</nav>
