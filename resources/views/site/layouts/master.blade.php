<!DOCTYPE html>
<html>
<head>
    <title>Draft Board | @yield('title')</title>

    {{-- Css Style --}}
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    {{-- Bootstrap JS --}}
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

</head>
<body>

    @include('site.includes.navbar')

<div class="container" style="padding-top:8px; margin-top:8px">
    @yield('content')
</div>


</body>
</html>
