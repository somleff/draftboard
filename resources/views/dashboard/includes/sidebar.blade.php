@inject('counter', 'App\Http\Services\CountService')

<div class="list-group">
    <a href="{{ route('main.index') }}" class="list-group-item list-group-item-action
        {{ Request::is('dashboard/main*') ? 'active' : '' }}">
        <i class="fas fa-home"></i>
        Main
    </a>

    <a href="/dashboard/blog" class="list-group-item list-group-item-action
        {{ Request::is('dashboard/blog*') ? 'active' : '' }}">
        Blog
    </a>

    <a href="{{ route('albums.index') }}" class="list-group-item list-group-item-action
        {{ Request::is('dashboard/albums*') ? 'active' : '' }}">
        <i class="fas fa-images"></i>
        Albums
    </a>

    <a href="{{ route('contact.index') }}" class="list-group-item list-group-item-action
        {{ Request::is('dashboard/contacts*') ? 'active' : '' }}">
        <i class="fas fa-file-signature"></i>
        Contacts
        <span class="badge badge-warning badge-pill">{{ $counter::execute() }}</span>
    </a>
</div>
