<form action="{{ route('contacts.destroy', $contact->id)}}" method="POST">
    @method('DELETE')
    @csrf
    <button class="btn btn-warning" type="submit">
        Delete
    </button>
</form>