@extends('dashboard.layouts.master')

@section('title', 'Contact Info')

@section('page_header')
    <h4>
        <i class="fas fa-eye"></i>
        {{ $contact->name }} Info
    </h4>
@endsection

@section('content')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header text-center">Contact Info</div>
            <div class="card-body">
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><b>Date:</b> {{ $contact->created_at }}</li>
                    <li class="list-group-item"><b>Name:</b> {{ $contact->name }}</li>
                    <li class="list-group-item"><b>Phone:</b> {{ $contact->phone }}</li>
                    <li class="list-group-item"><b>Email:</b> {{ $contact->email }}</li>
                    <li class="list-group-item"><b>Message:</b> {{ $contact->message }}</li>
                </ul>
            </div>

            <div class="card-footer">
                <div class="row justify-content-center">
                    {{-- Destroy View --}}
                    @include('dashboard.pages.contacts.destroy')

                    <a href="{{ url()->previous() }}" >
                        <input type="button" class="btn btn-outline-secondary ml-1" value="Back">
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection