@extends('dashboard.layouts.master')

@section('title', 'Contacts')

@section('page_header')
    <h4>
        <i class="fas fa-file-signature"></i>
        Contacts
    </h4>
@endsection

@section('content')
    @if ($contacts->count())
    <table class="table table-hover">
        <thead>
        <tr class="table-primary text-center">
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Message</th>
            <th scope="col">Action</th>
        </tr>
        </thead>

        <tbody>
            @foreach ($contacts as $contact)
                {{-- Define our css style for table --}}
                @php
                    if ($contact->viewed == 1) {
                        $class = "";
                    } else {
                        $class = "table-active";
                    }
                @endphp
                <tr class="text-center {{ $class }}">
                    <td>{{ $contact->name }}</td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->phone }}</td>
                    <td>
                        <a href="{{ route('contacts.show', $contact->id) }}" class="btn btn-link">
                            <i class="fas fa-eye"></i>
                        </a>
                    </td>
                    <td>
                        @include('dashboard.pages.contacts.destroy')
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-dismissible alert-light">
            <h4 class="alert-heading">We're sorry!</h4>
            <p class="mb-0">Today nobody wanna sent us their contact attributes.</p>
        </div>
    @endif
@endsection

@section('pagination')
        {{ $contacts->links() }}
@endsection
