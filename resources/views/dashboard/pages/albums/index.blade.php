@extends('dashboard.layouts.master')

@section('title', 'Albums')

@section('page_header')
    <div class="container">
        <div class="row">
            <h4>
                <i class="fas fa-images"></i>
                Albums
            </h4>
            <a href="{{ route('albums.create') }}" >
                <input type="button" class="btn btn btn-outline-warning ml-3" value="Create">
            </a>
        </div>
    </div>
@endsection

@section('content')
    @forelse($albums as $album)

    @empty
        <div class="alert alert-dismissible alert-light">
            <h4 class="alert-heading">We're sorry!</h4>
            <p class="mb-0">There is no albums yet, but you can create one.</p>
        </div>
    @endforelse
@endsection
