@extends('dashboard.layouts.master')

@section('title', 'Create New Album')

@section('page_header')
    <div class="container">
        <div class="row">
            <h4>
                <i class="fas fa-images"></i>
                Create New Album
            </h4>
            <a href="{{ url()->previous() }}" >
                <input type="button" class="btn btn btn-outline-secondary ml-3" value="Back">
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="card bg-light mb-3 mx-auto" style="max-width: 25rem;">
        <div class="card-body">
            <form method="POST" action="{{ route('albums.store') }}" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <div class="form-group">
                        <label for="name">
                            Name
                        </label>
                        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                               value="{{ old('name') }}" name="name" autofocus required>
                        {{-- Display errors --}}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="description">
                            Description
                        </label>
                        <textarea class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="6" required>{{ old('description') }}</textarea>
                        {{-- Display errors --}}
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="cover_img_path">Select a Cover Image</label>
                        <input type="file" class="form-control-file" name="cover_img">
                        {{-- Display errors --}}
                        @if ($errors->has('cover_img'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('cover_img') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="row justify-content-center">
                        <button type="submit" class="btn btn-warning">Create</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection
