@extends('dashboard.layouts.master')

@section('title', 'Edit Main Page Content')

@section('page_header')
    <h4>
        <i class="fas fa-pen-nib"></i>
        Edit Main Page Content
    </h4>
@endsection

@section('content')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header text-center">Change Info</div>
            <div class="card-body">
                <form method="POST" action="{{ route('main.update', $block->id) }}">
                    @method('PATCH')
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="title">
                                Title
                            </label>
                            <input type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                   value="{{ $block->title }}" name="title" autofocus required>
                            {{-- Display errors --}}
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="description">
                                Description
                            </label>
                            <textarea class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="3" required>{{ $block->description }}</textarea>
                            {{-- Display errors --}}
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="text">
                                Text
                            </label>
                            <textarea class="form-control {{ $errors->has('text') ? ' is-invalid' : '' }}" name="text" rows="3" required>{{ $block->text }}</textarea>
                            {{-- Display errors --}}
                            @if ($errors->has('text'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('text') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-warning">Update</button>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="card-footer">
                <div class="row justify-content-center">
                    <a href="{{ url()->previous() }}" >
                        <input type="button" class="btn btn-outline-secondary ml-1" value="Back">
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection