@extends('dashboard.layouts.master')

@section('title', 'Main')

@section('page_header')
    <h4>
        <i class="fas fa-home"></i>
        Main
    </h4>
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr class="table-primary text-center">
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Text</th>
            <th scope="col">Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($blocks as $block)
            <tr class="text-center">
                <td>{{ $block->title }}</td>
                <td>{{ $block->description }}</td>
                <td>{{ $block->text }}</td>
                <td>
                    <a href="{{ route('main.edit', $block->id) }}" class="btn btn-link">
                    <i class="fas fa-pen-nib"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
