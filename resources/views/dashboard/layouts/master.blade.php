<!DOCTYPE html>
<html>
<head>
    <title>Dashboard | @yield('title')</title>

    {{-- Css Style --}}
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    {{-- Bootstrap JS --}}
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    {{-- Font Awesome Icons --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

</head>
<body>

@include('dashboard.includes.navbar')

<div class="container" style="padding-top:8px; margin-top:8px">
    <div class="row">
        <div class="col-3 p-0">
            @include('dashboard.includes.sidebar')
        </div>

        <div class="col-9 m-0">
            <div class="card mb-3">
                <div class="card-header">
                    @yield('page_header')
                    @include('elements.status')
                </div>

                <div class="card-body">
                    <div class="row justify-content-center">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-3 p-0"></div>
        <div class="col-9 m-0 justify-content-end">
            @yield('pagination')
        </div>
    </div>
</div>

</body>
</html>
