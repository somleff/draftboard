<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(\App\Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'phone' => $faker->unique()->phoneNumber,
        'email' => $faker->unique()->email,
        'message' => $faker->unique()->text(150),
        'created_at' => Carbon::now()
    ];
});
