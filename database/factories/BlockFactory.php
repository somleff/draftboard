<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(\App\Block::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->text(20),
        'description' => $faker->unique()->text(80),
        'text' => $faker->unique()->text(150),
        'created_at' => Carbon::now()
    ];
});
