<?php

namespace App\Http\Controllers;

use App\Block;
use App\Http\Requests\BlockRequest;

class BlockController extends Controller
{
    protected $block;

    /**
     * Initialise model Block.
     *
     * @param Block $block
     */
    public function __construct(Block $block)
    {
        $this->block = $block;
    }

    /**
     * Show all Blocks on Dashboard.
     *
     * @return array $blocks
     */
    public function index()
    {
        $blocks = $this->block->all();

        return view('dashboard.pages.main.index', compact('blocks'));
    }

    /**
     * Display specified Block edit page.
     *
     * @param Block $block
     * @return array $block
     */
    public function edit(Block $block)
    {
        return view('dashboard.pages.main.edit', compact('block'));
    }

    /**
     * Update specified Block.
     *
     * @param BlockRequest $request
     * @param Block        $block
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BlockRequest $request, Block $block)
    {
        $block->update($request->all());

        return redirect()->route('main.index')
            ->with('status', __('app.block_update'));
    }
}
