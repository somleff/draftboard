<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactsRequest;

class ContactController extends Controller
{
    protected $contact;

    /**
     * Initialise model Contact.
     *
     * @param Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }
    
    /**
     * Send Contact.
     *
     * @param ContactsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContactsRequest $request)
    {
        $this->contact->create($request->all());

        return redirect()->back()
            ->with('status', __('app.contacts_store'));
    }

    /**
     * Show all Contacts on Dashboard.
     *
     * @return array $contacts
     */
    public function index()
    {
        $contacts = $this->contact->paginate(3);

        return view('dashboard.pages.contacts.index', compact('contacts'));
    }

    /**
     * Show specified Contact on Dashboard
     *
     * @param Contact $contact
     * @return array $contact
     */
    public function show(Contact $contact)
    {
        $contact->viewed();

        return view('dashboard.pages.contacts.show', compact('contact'));
    }

    /**
     * Delete specified Contact on Dashboard.
     *
     * @param Contact $contact
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();

        return redirect()->route('contact.index')
            ->with('status', __('app.contacts_destroy'));
    }
}
