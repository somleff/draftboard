<?php

namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests\AlbumRequest;

class AlbumController extends Controller
{
    protected $album;

    /**
     * Initialise models.
     *
     * @param Album $album
     */
    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    /**
     * Show all Albums on Dashboard.
     *
     * @return array $albums
     */
    public function index()
    {
        $albums = $this->album->all();

        return view('dashboard.pages.albums.index', compact('albums'));
    }

    /**
     * Show Album creating form.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('dashboard.pages.albums.create');
    }

    /**
     * Store New Album.
     *
     * @param AlbumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AlbumRequest $request)
    {
        $this->album->createAlbum($request);

        return redirect()->route('albums.index')
            ->with('status', __('app.album_store'));
    }
}
