<?php

namespace App\Http\Controllers;

use App\Block;
use App\Album;

class PagesController extends Controller
{
    protected $block;
    protected $album;

    /**
     * Initial models.
     *
     * @param Album $album
     * @param Block $block
     */
    public function __construct(Block $block, Album $album)
     {
         $this->block = $block;
         $this->album = $album;
     }

    /**
     * Display view for Main page.
     *
     * @return array $blocks
     */
    public function main()
    {
        $blocks = $this->block->all();

        return view('site.pages.main', compact('blocks'));
    }

    /**
     * Display view for Blog page.
     *
     * @return \Illuminate\View\View
     */
    public function blog()
    {
        return view('site.pages.blog');
    }

    /**
     * Display view for Gallery page.
     *
     * @return array $albums
     */
    public function albums()
    {
        $albums = $this->album->paginate(3);

        return view('site.pages.albums', compact('albums'));
    }

    /**
     * Display view for ContactUs page.
     *
     * @return \Illuminate\View\View
     */
    public function contactUs()
    {
        return view('site.pages.contacts');
    }
}
