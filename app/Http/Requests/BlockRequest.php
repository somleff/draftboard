<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [ 'string', 'min:8', 'max:20'],
            'description' => ['required', 'string', 'min:20', 'max:80'],
            'text' => ['required', 'string', 'min:25', 'max:150'],
        ];
    }
}
