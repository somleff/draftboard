<?php

namespace App\Http\Services;

use App\Contact;

class CountService
{
    /**
     * Counting an incoming Contacts.
     *
     * @return integer
     */
    public static function execute()
    {
        $counter = Contact::select('message')->where('viewed', 0)->count();

        return $counter;
    }
}
