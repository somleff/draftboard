<?php

namespace App\Http\Services;

class GetMainImageService
{
    /**
     * Get title for  main album photo.
     *
     * @param $request
     * @return string
     */
    public static function execute($request)
    {
        $file = $request->file('cover_img');
        $extension = $file->getClientOriginalExtension();
        $title = uniqid(false) . '.' .$extension;

        $file->move(storage_path(). '/app/public/albums/', $title);

        return 'albums/'.$title;
    }
}
