<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Services\GetMainImageService;

class Album extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'cover_img_path'];

    /**
     * An Album can have many images.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    /**
     * Create new Album in DB.
     *
     * @param $request
     * @param $imgPath
     * @return object
     */
    public function createAlbum($request)
    {
        return $this->create([
            'name' => ucwords($request['name']),
            'description' => $request['description'],
            'cover_img_path' => GetMainImageService::execute($request),
        ]);
    }
}
