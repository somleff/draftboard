<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'phone', 'message'];

    // When we click on the [eye] icon,
    // script marks the Contact as
    // viewed automatically.
    public function scopeViewed($query)
    {
        return $query->where('id', $this->id)
            ->update(['viewed' => true]);
    }
}
