## Task 4 Dmi3yy  
  
 Структура сайта: 
- Главная 
- Блог + 2-3 записи в нем 
- Галерея (разделы + картинки в них)
- Контакты с формой обратной связи  

- Шапка: Верхнее меню  + Хлебные крошки
- Главная: На главной вывод 3-4 блоков типа наши преимущества и просто блок с контентом 
- Блог: вывод статей  + обрезать картинку в нужный размер(100x100px) а пользователь может грузить любую 
- Галерея: вывод альбомов + картинок в них
- Контакты: текстовый блок + форма обратной связи 
- Admin dashboard  

**Commit History**  
_Initial Project_ - install Laravel 5.7  
  
_Blade&Bootstrap_ - creating folder hierarchy for blade templates, add bootswatch:Litera theme. Time: 2.5 h  
  
_Contact Page_ - creating table, model, controller for Contacts. CRD methods with message('status'). `Request/ContactRequest` for validation. Time: 2.5 h  
  
_Counter_ - add `CountContact` middleware. Make dashboard views more pretty. Create ` ContactFactory` and `ContactTableSeeder` for testing purposes. Time: 2.5 h  
  
_Main Block_ - create `BlockTableSeeder` with `create_blocks_table` migration to fill in Main page with fake content. Create `PageController` for display pages in Site. Create `BlockController` for U method with message('status''). 
`Request/BlockRequest` for validation. Time: 2 h  
  
_Albums_ - create `albums`, `images` table migration. Create models `Album`, `Image` and controller for them. Set relations between them. Create method `storeMainAlbumImage` for upload main image for `Album`.  
  
_Service Class_ - add some refactor with `GetMainImageService`.  
  
_Hello inject_ - create `CountService` in `Services` folder. Use `@inject` in `sidebar.blade.php`. Delete `CountContacts` middleware.
  


  
   